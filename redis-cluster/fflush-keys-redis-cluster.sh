#!/bin/bash

redis-cli -h 192.168.80.10 -p 7000 -c CLUSTER NODES \
    | awk -F'[ @:]' '/master/{print "-h "$2" -p "$3}' \
    | xargs -I{} sh -c 'echo node {};redis-cli {} FLUSHALL'
