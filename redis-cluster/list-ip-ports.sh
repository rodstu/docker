#!/bin/bash


seq 0 5 \
    | xargs -I{} docker inspect redis-{} \
    | jq '.[] 
        | {
            IP: .NetworkSettings.Networks | ."redis-cluster_redis_net" | .IPAddress,
            Name: .Name,
            Port: .NetworkSettings.Ports["6379/tcp"][0].HostPort
        } '

echo ""
echo "to facilitate your life:"
echo ""

seq 0 5 \
    | xargs -I{} docker inspect redis-{} \
    | jq -r '.[] 
        | "\(.NetworkSettings.Networks | ."redis-cluster_redis_net" | .IPAddress):\(.NetworkSettings.Ports["6379/tcp"][0].HostPort)"' \
    | paste -d" " -s
