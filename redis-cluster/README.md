# Step 1

Run `docker compose up -d`


# Step 2 (only once)

For the first run, you need to "create" the cluster associations

Run from your terminal

```sh
./list-ip-ports.sh
```

the output of the last line should be:

    192.168.80.10:7000 192.168.80.11:7001 192.168.80.12:7002 192.168.80.13:7003 192.168.80.14:7004 192.168.80.15:7005

access a bash from a running redis instance:

    docker exec -it redis-0 bash

create the cluster

    redis-cli --cluster create 192.168.80.10:7000 192.168.80.11:7001 192.168.80.12:7002 192.168.80.13:7003 192.168.80.14:7004 192.168.80.15:7005 --cluster-replicas 1

type `yes` in the question

# Step 3 (check)

Checks if everything is up and runing


    redis-cli -h 192.168.80.10 -p 7000
    > CLUSTER INFO
    > CLUSTER NODES
