#!/usr/bin/env bash


docker run --name mysql57 \
    -e MYSQL_ALLOW_EMPTY_PASSWORD=1 \
    -e MYSQL_ROOT_PASSWORD="" \
    -v /home/rods/.cache/mysql57-data:/var/lib/mysql \
    -p 3306:3306 \
    -d mysql:5.7 --max-allowed-packet=67108864
