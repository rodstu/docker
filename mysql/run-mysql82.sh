#!/usr/bin/env bash


docker run --name mysql82 \
    -e MYSQL_ALLOW_EMPTY_PASSWORD=1 \
    -e MYSQL_ROOT_PASSWORD="" \
    -v /home/rods/.cache/mysql82-data:/var/lib/mysql \
    -p 3307:3306 \
    -d mysql:8.2.0 --max-allowed-packet=67108864
